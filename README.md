# Introducción a la Terminal y Línea de Comandos (LINUX) <a name="inicio"/>

## Contenido
* 1. [Línea de Comandos](#Linea-Comandos)
* 2. [Sistema de Archivos ](#Archivos)
* 3. [Utilidades Interactivas](#Utilidades-interactivas)
* 4. [Utilidades Batch](#Utilidades-batch)
* 5. [Flujos Estándar](#Flujos-estándar)
* 6. [Procesos en background y foreground](#Procesos-background-foreground)
* 7. [Permisos sobre archivos: El sistema de permisos octal ](#Permisos)
* 8. [Sitemas de manejo de paquetes](#Paquetes)
* 9. [Herramientas de compresión y combinación de archivos](#Compresion)
* 10. [Herramientas de búsqueda de archivos](#Busqueda)
* 11. [ Herramientas para interactuar a través de HTTP](#HTTP)
* 12. [Acceso seguro a otras computadoras](#ssh)
* 13. [Servicio de mailing](#Mailing)
* 14. [Variables de entorno](#Variables-entorno)
* 15. [Automatización de tareas](#Automatización)

---
## Línea de Comandos <a name="Linea-Comandos"/> [↑](#inicio)
### Comandos
Consiste en *el nombre de un programa, los parámetros y los modificadores*, todos separados por un espacio. La diferencia entre parámetros y modificadores es que los modificadores alteran el comportamiento del programa mientras que los parámetros solo son información adicional para la ejecución del programa.

**Esquema general:** `comando -flag1 -flag2 arg1 arg2` 

#### Lista de comandos básicos:
Comando | Descripción
--------|------------
`date` | Muestra la fecha (Se utiliza sin ningún parámetro).
`echo ` | Muestra un mensaje en la pantalla, o en su defecto el valor del parámetro señalado.
`man` | Comando "manual" que muestra información sobre otros comandos. Recibe como parámetro otro comando para el cual se brindará la información. Por ejemplo `man date`.
`ls` | Muestra los archivos contenidos en el directorio.
`ls -a` | Muestra todos los archivos contenidos en el directorio, incluyendo los archivos ocultos y punteros a directorios (".." -> directorio padre,  "." -> directorio actual).
`ls -l` | Muestra detalles de los archivos contenidos en el directorio (usuario, grupo, permisos, tamaño, fecha y hora de creación).
`ls -lh` | Muestra la misma información que ls -l pero con las unidades de tamaño en KB, MB
`ls -t` | Ordena los archivos por fecha de modificación.
`ls -R`| Muestra el contenido de todos los subdirectorios de forma recursiva
`ls -S` | Ordena los resultados por tamaño de archivo
`ls -x` | Ordena elementos primero por nombre y después por extensión.
`ls -X` | Ordena los elementos primero por extensión y luego por nombre.
`pwd` | Muestra el path del directorio actual (/directorio/subdirectorio/nombreDelDirectorio).
`cd` | Cambia de directorio. Recibe como parámetro el directorio al que se moverá.
`cd ~` | Cambia directamente a home (**Atajo**).
`cd -` | Cambia al último directorio visitado (**Atajo**).
`cd ..` | Cambia al directorio anterior.
`mkdir` | Crea un directorio. Recibe como parámetro el nombre del nuevo directorio.
`cp` | Copia un archivo a cierta ubicación. Recibe dos parámetros, el primero es el archivo a copiar y el segundo es el path del directorio destino. Por ejemplo `cp test.txt test/`.
`rm` | Elimina el archivo seleccionado (**Solo elimina archivos, no directorios**). Recibe como parámetro el nombre del archivo a eliminar.
`mv` | Mueve (Copia y elimina) un archivo a cierta ubicación (Solo mueve archivos, no directorios). Recibe dos parámetros, el primero es el archivo a copiar y el segundo es el path del directorio destino. Por ejemplo `mv test.txt assets/files/test/`.
`rmdir` | Elimina el directorio seleccionado (**No se puede borrar un directorio con archivos dentro**). Recibe como parámetro el nombre del directorio a eliminar.

#### Atajos
Tecla(s) | Descripción
---------|------------
**tab** | Autocompleta, o en el caso de los comandos muestra todas las opciones que comienzan con las letras ingresadas antes de teclear *tab*
**↑↓** | Navegación en el historial de los comandos utilizados.
**Ctrl + Shift + r** | Busqueda hacía atras de los comandos anteriormete escritos.
**history** | Permite ver todos los comandos escritos alguna vez. Para ejecutar algún comando del historial se debe escribir un signo de admiración *!*, seguido del número indicado a la izquierda del comando. por ejemplo: `!10`


---
## Sistema de Archivos <a name="Archivos"/> [↑](#inicio)
### Tipos de archivo
* Binarios. Archivos que están escritos para ser interpretados por una computadora. Ejemplos: Programas ejecutables, fotos, vídeos, etc.
* De texto. Archivos también binarios, sin embargo los binarios que los componen corresponden a caracteres que al abrirlos en un editor de texto son entendibles por humanos. Por ejemplo, archivos de configuraciones, páginas web, o códigos fuente.


---
## Utilidades interactivas <a name="Utilidades-interactivas"/> [↑](#inicio)
Programas que se ejecutan inmediatamente al correr el comando, mostrando un resultado en pantalla al instante.
Las utilidades interactivas (editores de texto) más conocidas son *vim* y *nano*.

### vim
`vim 'archivo_a_ejecutar'`. Por ejemplo: `vim test.txt`

#### Comandos
Comando* o Tecla(s)** | Descripción
-------------------|------------
`i` * | Permite insertar texto. (Cambia al modo de inserción)
*esc* ** | Permite salir del modo edición.
*:* ** | Activa el modo de comandos.
*:w* ** | Guarda el contenido del archivo.
*:q* ** | Salir del vim.
*:x* ** | Guardar el archivo y salir de vim.

#### Creación de un archivo vim
`vim 'archivo_a_crear'`. Por ejemplo: `vim new.txt`

### nano
`nano 'archivo_a_ejecutar'`. Por ejemplo: `nano test.txt`

A diferencia de vim, en nano, aparecen ayudas visuales con los comandos que se pueden utilizar, donde el simbolo '^' hace referencia a la tecla *Ctrl*. Otra diferencia se observa al ingresar a la pantalla "nano", el entorno ya se encuentra en modo edición.

#### Creación de un archivo nano
`nano 'archivo_a_crear'`. Por ejemplo: `nano new.txt`


---
## Utilidades Batch (Procesamiento por lotes) <a name="Utilidades-batch"/> [↑](#inicio)
### Trabajo fundamental con archivos de texto
Utilidad Batch | Descripción
---------------|------------
**touch** | Nos permite crear archivos. Ejemplo: `tocuh file.txt`
**cat** | Nos muesta el contenido completo de un archivo. Recibe como parámetro el nombre del archivo a mostrar. Ejemplo: `cat batch.txt`
**head** | Muestra solamente las primeras líneas del archivo (por defecto solo mostrará las primeras 10 líneas), aunque también se puede elegir cuantas líneas queremos que se muestren. Recibe como parámetro el nombre del archivo y para elegir un número de líneas se utiliza el modificador *-n* seguido del número de líneas. Ejemplos: `head batch.txt` y `head -n 5 batch.txt`
**tail** | Muestra solamente las últimas líneas del archivo (por defecto solo mostrará las últimas 10 líneas), aunque también se puede elegir cuantas líneas queremos que se muestren. Recibe como parámetro el nombre del archivo y para elegir un número de líneas se utiliza el modificador *-n* seguido del número de líneas. Ejemplos: `tail batch.txt` y `tail -n 5 batch.txt`

### Búsqueda y tratamiento de texto
Utilidad Batch | Descripción
---------------|------------
**grep** | Busqueda por expresiones regulares (o por "palabra clave"), es decir mostrará las líneas del archivo que coincidan con la expresión regular (regex) que se desee utilizar. La *regex* se escribe seguida del comando `grep` y a continuación se escribe como parámetro el nombre del archivo a consultar. Ejemplos: `grep New json.txt`, `grep -i "new'),$" json.txt`. También se pueden utilizar operadores y modificadores (o flags) como: <ul><li>El modificador *-i* para no hacer distinción entre mayúsculas o minúsculas. Ejemplo: `grep -i new json.txt`; o </li><li>El operador *$* ubicandolo al final de la "palabra clave" que se desea buscar para verficar si la línea incluye esta palaba al final. Ejemplo: `grep -i "new$" json.txt`; o </li><li>El operador *^* para revisar si incluye la "palabra clave" al principio. Ejemplo: `grep -i "^new" json.txt`</li></ul>
**sed** | Tratamiento de flujos (**sed = Stream editor**), también utiliza expresiones regulares (regex) y permite reemplazar una regex por otra, por ejemplo para reemplazar cierto texto por uno nuevo se puede hacer de la siguiente manera: Utilizando el comando *s/*, que inicia la sustitución del (los) elemento(s), seguido del elemento a modificar, el simbolo */*, el nuevo elemento y por último, el modificador */g*, indicando que el cambio se realizará a lo largo de todo el flujo, o globalmente, o sustituyendo este modificador, se puede escribir el número "índice" de la aparición, por ejemplo si se busca la cuarta aparación se agrega un */4*. Ejemplo: `sed 's/New/Old/g' json.txt`. <ul><li>**NOTAS:** </li><ol><li> EL COMANDO `sed` POR SI MISMO, NO MODIFICA EL ARCHIVO. Lo que hace es modificar el flujo creando un nuevo flujo con la modificación. </li><li> Si se desea cambiar/sustituir mas de un elemento, se puede hacer, separando las peticiones con un `;`. Ejemplo: `sed 's/New/Old/;s/1/2/g' json.txt`</li></ol></ul> Otro tramiento de flujo permite eliminar la última línea del archivo a tratar utilizando el modificador *'$d'* seguido del nombre del archivo, como argumento (parámetro). Ejemplo: `sed '$d' json.txt`. El comando `sed` tiene muchas más utilidades, siempre para trabajar en archivos de texto en modo *"procesamiento por lotes"*; sus aplicaciones son muy interesantes para cuando se quiere aplicar una modificación muy puntual a un archivo muy grande y especíalmente cuando esa modificación tiene que hacerse sobre muchos archivos similares.
**awk** | Tratamiento de texto delimitado; también es utilizado para el tratamiento de texto; este comando funciona muy bien para trabajar con textos estructurados, como por ejemplo, archivos separados por comas o por tabs, etc. Ejemplo: `awk -F ':' '{ print $1 }' json.txt `, en este caso el delimitador para separar las columnas del archivo será `':'`, y agregamos el comando de scripting `({ print $1 })`, escrito entre comillas, (**NOTA:** Este comando de scripting también se puede utilizar en el comando `sed`), que imprime solo la primera columna del archivo seleccionado, *json.txt*. `awk` también permite hacer ciertas condiciones para la ejecución del comando, incluyendo calculos `awk -F ';' 'NR > 1 && $3 > 0 { print $1, $3 * $4 }' awk.txt` en este caso revisamos que el Número de Línea (NR) sea mayor que uno y que la tercer columna sea mayor que cero, si esto se cumple entonces se imprimira la columna uno y la columa tres multiplicada por la columna cuatro.

### Ejemplos [→](./assets/files)
Dentro del directorio *assets/files* se encuentran archivos de texto como práctica de estos comandos.

### Bibliografía:
#### Material de apoyo:
* Utilidades de administración de un sistema LINUX. https://www.efectodigital.online/single-post/2019/06/19/Utilidades-de-administraci%C3%B3n-de-un-sistema-LINUX
* Más usos del comando `sed` en este tutorial: https://likegeeks.com/es/sed-de-linux/.


---
## Flujos Estándar <a name="Flujos-estándar"/> [↑](#inicio)
En Linux/Unix cada nuevo proceso generado (cada aplicación ejecutada) se inicializa con tres canales de datos (**streams**): 
1. **Standar Input**: Entrada de datos del programa.
2. **Standar Output**: Mensajes para ser visualizados por el usuario. Estos mensajes no deden considerar advertencias o errores.
3. **Standar Error**: Mensajes de advertencias o errores que pueden suceder durante la ejecución del programa.

Normalmente los *Input* o los *Output* son mediante el teclado y el monitor, respectivamente, pero pueden cambiar y ser un archivo o incluso otro proceso.

### Comandos de Input y Output
Comando | Ejemplo | Descripción
--------|---------|------------
`'comando' > 'nombreArchivo' ` | `history > lastCommands.txt` | Redirecciona la salida de un *comando* con el contenido de un *archivo*, es decir, se puede **almacenar** la salida del comando en un archivo. **Si el archivo ya existe, se sobreescribe el contenido, elimando lo que este tenía**
`'comando' >> 'nombreArchivo' ` | `history >> lastCommands.txt` | Redirecciona la salida de un *comando* con el contenido de un *archivo*, es decir, se puede **concatenar** la salida del comando al final del archivo agregado, **sin perder el contenido que este pudiera almacenar.**
`'comando' < 'nombreArchivo' ` | `mysql -h 127.0.0.1 -u root -p1234 < dump1.sql` | Redirecciona la entrada de un *comando* con el contenido de un *archivo*, es decir, ejecuta el (los) comando(s) del archivo a partir del comando. Útil para procesar multiples comandos o un comando muy grande y eliminar la necesidad de escribirlo(s) constantemente. También permite colocar todo el contenido del archivo como un *Standar Input*  del comando, por ejemplo: `grep < lastCommands.txt "rm"`.

### Pipes "|"
Permiten hacer procesamientos muy complejos de una manera muy sencilla, donde la salida de un proceso puede ser la entrada de otro proceso, y la salida de este, a su vez, puede ser la entrada de un tercer proceso.

**Ejemplos:**

Comando | Descripción
--------|------------
`ls -l \| more` | *more*, muestra el resultado largo de un comando en secciones. Presionando 'Enter' muestra el siguiente resultado línea por línea, presionando la barra espaciadora ('Space') muestra otra pantalla o sección
`history \| grep "rm"` | Mostrará los últimos comandos que cuentan con "rm" en su ejecución.
`cat dump1.sql \| wc -l`| *wc*, workCount cuenta los caracteres, palabras o líneas que hay en un archivo o en un flujo. *-l*, indica que se contarán las líneas del archivo. 

Así como en los ejemplos se encadeno la ejecución de dos comandos, también se puede hacer con tres, cinco, o con la n cantidad necesaria.

### [Práctica](./assets/files/practices/flujosEstándar.md)

### Bibliografía:
* Flujos estándar y cómo los Pipes nos pueden dar super poderes. https://platzi.com/tutoriales/1748-terminal/4661-flujos-estandar-y-como-los-pipes-nos-pueden-dar-super-poderes/


---
## Administración de procesos en background y foreground <a name="Procesos-background-foreground"/> [↑](#inicio)
El ejecutar un comando equivale a lanzar un proceso nuevo que en seguida se comienza a ejecutar.

Existen dos tipos o modalidades de ejecución:
* **Ejecución en primer plano o *Procesos Foreground*:** Son aquellos procesos que mientras estén en ejecución, la terminal no permitirá realizar otra acción. 
* **Ejecución en segundo plano o *Procesos Background*:** Procesos que se están ejecutando mientras la terminal nos permite realizar otras actividades. 

### Procesos Background 
Lograr la ejecución de los procesos en segundo plano se puede llevar a cabo de dos maneras:
1. **Desde el inicio de su ejecución:** se debe ingresar el símbolo *'&'* al final del comando. Por ejemplo: `mysql -h 127.0.0.1 -u root -p1234 < dump1.sql &`; y
2. **Durante su ejecución:** presionando las treclas *'ctrl + z'*. 

**Para volver a colocar un proceso en primer plano, se debe ingresar el comando** `fg`. 

Un ejemplo de un proceso ejecutandose en background constantemente es el de un servidor que debe estar ejecutandose siempre para que el sistema funcione, a este tipo de procesos se le conoce como **Servicios** o **Demoneos (Demons)**.

### Administración de procesos
**¿Cuales son los procesos que están en ejecución?**
Para estos tenemos dos herramientas (Existen más pero estás son las comunes):

`ps`: Al ejecutarlo responde con los procesos que estan en ejecución. Se puede agregar el modificador *'ax'*: `ps ax` que muestra los procesos que corren en el sistema. **Esta utilidad es de tipo Batch, por lo tanto permite el uso de Pipes "|".**

`top`: Es una utilidad interactiva, que indica en tiempo real como los procesos van cambiando, los recursos que utiliza el equipo e inclusive permite finalizar los procesos del sistema o visualizar más información.
Para salir de la utiliería presionar *q*.

**¿Como detener un proceso?**
* Si el proceso esta en *Foreground* con *'ctrl + c'* se puede finalizar.
* Pero si esta en *Background*, primero se debe identificar el número de PID que diferencia nuestro proceso a cortar; a través del comando `ps ax` y segundo: se debe utilizar alguno de los siguientes comandos para detener el proceso:

Comando | Estructura | Ejemplo | Descripción
--------|------------|---------|------------
`kill` | `kill -9 'No. PID del proceso'` | `kill -9 13063` | Detiene la ejecución del proceso indicado a partir de su Identificador de proceso (PID). El *-9* indica que se debe detener el proceso de manera inmediata, pero no es necesario agregar este modificador
`killall` | `killall 'Nombre del proceso'` | `killall excel` | Detiene la ejecución del proceso indicado a partir de su Nombre.


---
## Permisos sobre archivos: El sistema de permisos octal <a name="Permisos"/> [↑](#inicio)

Unix fue diseñado como un sistema multiusuario, implicando que ciertas cuestiones básicas de seguridad estén intrinsecas dentro del sistema; una de estas cosas, por ejemplo, son los archivos pertenecientes a un usuario, ya que no pueden ser accedidos por otro usuario, salvo que el primer usuario le de los permisos necesarios.

En todos los sistemas existe el usuario **root**, que es un superusuario, es el Administrador del Sistema, algo así como un usuario Dios.

Todos los archivos de Unix tienen tres **usuarios**:
* Un *dueño (**u**)* asociado, creador del archivo, 
* Un *grupo (**g**)* de usuarios que pueden accederlo y
* *Otros (**o**)* usuarios a los que se les puede dar permisos distintos.

Las **operaciones** que pueden hacerce a un archivo son tres: *Lectura*, *Escritura* y *Ejecución*. 

### Permisos
Los usuarios y las operaciones "configuran" una matriz de **permisos**, donde se pueden observar las operaciones que podría realizar cierto usuario al archivo.

Al correr el comando `ls -l`, se pueden visualizar los permisos de *Lectura (**r**)*, *Escritura (**w**)* y *Ejecución (**x**)* para cada uno de los usuarios (grupos de tres caracteres (rwx)) *dueño* (posición 1), *grupo* (posición 2) y *otros* (posición 3) en la primer columna de la salida del comando; el primer carácter de esta columna indica el tipo de archivo, por ejemplo: *d* indica que el archivo es un directorio, *-* para archivos comunes y *l* para links (puntero a otro archivo). Cuando uno de los permisos (rwx) es sustituido por un *'-'* significa que no se puede utilizar ese permiso. 

**Ejemplos:**

![Ejemplo de permisos Comando 'ls -l'](./assets/images/permisosComando_ls-l.jpg "Permisos Comando 'ls -l'")
 
Para alterar los permisos que tiene un archivo asociado se tiene los siguientes comandos:

Comando | Descripción | Ejemplos
--------|-------------|---------
`chmod` | Cambia el modo del archivo, en definitiva es cambiar individualmente los permisos. | <ul><li>`chmod o-w file1.txt` Quita el permiso de *Escritura* a *Otros* para el archivo *file1.txt*</li><li>`chmod +x practice.txt` Agrega el permiso de *Ejecución* a cualquiera de los tres usuarios para el archivo *practice.txt*</li></ul> **NOTA: estos permisos fueron cambiados a partir de la *Notación textual***
`chown` | Cambia quien es el usuario propretario del archivo. **NOTA: Solo el usuario *root*, o al anteponer el `sudo` sobre el comando, pueden hacer uso de este comando.**  | `sudo chown newUser file1.txt`
`chgrp` | Cambia quien es el grupo de usuarios que puede acceder al archivo. **NOTA: Solo el usuario *root*, o al anteponer el `sudo` sobre el comando, pueden hacer uso de este comando.** | `sudo chgrp newUser file1.txt`

**NOTA:** `sudo`, permite ejecutar comandos con permisos "especiales", permisos de usuario *root*.

#### Notación binaria
Los permisos son flags, o tengo el permiso o no lo tengo, al combinar los permisos (de cada una de las letras) con '1' y '0', se forman números binarios, por ejemplo:

| r | w | x | |
| --|---|---|-|
| 1 | 0 | 0 | 4 |
| 1 | 1 | 0 | 6 |
| 1 | 1 | 1 | 7 |

Esto pasa para cada uno de los permisos disponibles, de modo que se obtiene la siguiente matriz, que termina configurando un número decimal del tres dígitos.

| Dueño | Grupo | Otros |-|
|-------|-------|-------|-|
| <table><thead><tr><td>r</td><td>w</td><td>x</td></tr></thead><tbody><tr><td>1</td><td>1</td><td>0</td></tr><tr><td>1</td><td>0</td><td>0</td></tr><tr><td>1</td><td>1</td><td>1</td></tr></tbody></table> | <table><thead><tr><td>r</td><td>w</td><td>x</td></tr></thead><tbody><tr><td>1</td><td>0</td><td>0</td></tr><tr><td>0</td><td>0</td><td>0</td></tr><tr><td>1</td><td>1</td><td>1</td></tr></tbody></table> | <table><thead><tr><td>r</td><td>w</td><td>x</td></tr></thead><tbody><tr><td>0</td><td>0</td><td>0</td></tr><tr><td>0</td><td>0</td><td>0</td></tr><tr><td>1</td><td>1</td><td>1</td></tr></tbody></table> | <table><thead><tr><td>-</td></tr></thead><tbody><tr><td>640</td></tr><tr><td>400</td></tr><tr><td>777</td></tr></tbody></table>

Las notaciones binarias son utilizadas para hacer cambios de permisos a varios usuarios en una sola operación; por ejemplo: `chmod 760 file1.txt`. Indicando que el *Dueño* del archivo *file1.txt'* tendrá los tres permisos, el *Grupo* tendrá permiso de escritura y de lectura y *Otros* no tendrán ningún permiso sobre el mismo archivo.

### [Práctica](./assets/files/practices/permisos.md)

### Bibliografía:
#### Material de apoyo:
* Domina la Administración de Usuarios y Permisos en Servidores Linux. https://platzi.com/blog/administracion-usuarios-servidores-linux/


---
## Sitemas de manejo de paquetes <a name="Paquetes"/> [↑](#inicio)
Para poder utilizar un software que no viene con el sistema de fabrica es necesario instalarlo. 
Una instalación implica conocer el lugar de donde se descargara el programa, descargar el programa, mover los archivos del programa a los lugares donde sea necesario y por último hacer alguna configuración para que el programa pueda ser utilizado en la computadora. Existen programas que se encargan de realizar todas estas tareas, llamados **Paquetes de Software**, así como existen estos paquetes, también existen los **Administradores de Paquetes** que conocen, por ejemplo: de donde realizar las descargas, que otros paquetes ya están instalados en nuestro sistema y como configurar todo de modo que no haya conflictos, muchas veces, algún paquete depende de la existencia prevía de otro.

### Paquetes binarios
Dependiendo del sistema operativo que se tenga instalado se podrán utilizar diferentes administradores de paquetes:
* **apt**. Se utiliza en distribuciones de Linux basadas en Debian como Ubuntu.
* **zypper**. Se utiliza en las distribuciones de Suse, y en algunas otras.
* **rpm**. Es universal.

### Comando apt
Comando | Ejemplo | Descripción 
--------|---------|------------
`apt install 'nombreDelPrograma` | `sudo apt install lynx` | Instalará el programa *lynx* que es un navegador de internet de línea de comandos. Ejemplo de uso de `lynx`: `lynx vainilladev.com`

### Paquetes de lenguajes
Existen diferentes paquetes para los diferentes lenguajes de programación, estos paquetes son librerias escritas en el mismo lenguaje que se utilizará.
Cada lenguaje propone su propio manejador de paquetes:
* *pip* para Python, ejemplo de instalación de una librería: `sudo pip install pandas`
* *composer* para PHP y
* *npm* para Node.js

### Otros paquetes
Existen otros manejadores de paquetes que pretenden ser mas genéricos que permiten instalar tanto *paquetes binarios* o de *lenguajes*, entre estos se encuentran: **conda** y **homebrew**.


---
## Herramientas de compresión y combinación de archivos <a name="Compresion"/> [↑](#inicio)
### Compresión
La compresión de archivos se utiliza para enviar archivos de un equipo a otro o para tener copias de seguridad (backups) de manera que se puedan optmizar y aprovechar los recursos, esto debido a la disminución en el tamaño (espacio de memoria) de los mismos.

Comando | Descripción
--------|------------
`ls dump.sql -l` | Para ver los detalles, y la memoria, del archivo *dump*. Si se agrega el modificador `-h` de `ls` se muestra el mismo resultado, pero de una forma más legible (`ls dump.sql -lh`).
`gzip dump.sql` | Comprime el archivo seleccionado, creando un nuevo archivo de extensión *gz* (*dump.sql.gz*)
`gzip -d dump.sql` | Descompirme el archivo, obteniendo el archivo original

### Combinación
Permite agrupar archivos en uno solo.

Comando | Descripción
--------|------------
`tar cf backup.tar backup/*` | Empaqueta los archivos de la carpeta *backup* en un solo archivo de extension *tar*. **NOTA:** `cf` significa "Create File".
`tar tf backup.tar` | Muestra el contenido del archivo *backup.tar*.
`tar xf backup.tar` | Desagrupa el contenido del archivo *backup.tar*.
`tar -cvf backup.tar backup/*` | Empaqueta los archivos de la carpeta *backup* y muestra su contenido.
`tar czf backup.tgz backup/*` | Empaqueta los archivos y comprime el empaquetado de los archivos de la carpeta *backup*.
`tar xzf backup.tgz` | Descomprime el archivo *backup.tgz* y desempaqueta los archivos.


---
## Herramientas de búsqueda de archivos <a name="Busqueda"/> [↑](#inicio)
Las principales herramientas para la búsqueda de archivos son:

### locate
Permite hacer una búsqueda en todo el sistema de archivos simplemente agregando el nombre del archivo deseado. Ejemplo: `locate something.php`. **NOTA: `locate` funciona mediante una Base de Datos que debe ser explicita y periodicamente actualizada, de lo contrario, los archivos no se encontrarán. Para hacer esta actualización se debe correr el comando `sudo updatedb`**. 

### whereis
Utilizada para la búsqueda de archivos binarios, es decir, comandos, Ejemplo: `whereis echo`. La respuesta a este comando es la ubicación de `echo` y la ubicación del manual de `echo`.

### find
Es la herramienta más compleja debido a que busca dentro de un árbol de directorios utilizando una serie de criterios, esto según lo indicado en el mismo comando. Ejemplos:
* `find . -user miguel -perm 644`. Busca dentro del *Directorio actual* los archivos que correspondan al usuario *miguel* y que tengan los permisos *644*.
* `find . -type f -mtime +7`. Busca dentro del *Directorio actual* solo los *archivos* que hayan sido modificados hace *más de siete días*. En la bandera `-type`, `f` corresponde a *Regular File*, es decir solo buscará archivos, mientras `d` corresponde a *Directorios*. 
* `find . -type f -mtime +7 -exec cp {} ./backup/ \;`. Busca dentro del *Directorio actual* solo los *archivos* que hayan sido modificados hace *más de siete días*. Con el comando `find` se puede ejecutar alguna operación sobre los archivos encontrados, para eso se utiliza **`exec`**; en el ejemplo anterior, la operación `-exec` corresponde a copiar los archivos `{}` (refiriendose a todos los archivos encontrados en la búsqueda) y los pegará en la carpeta *backup*. Nótese que el comando finaliza con un `;` (`\;`).

#### NOTAS
* Si no se indica una ruta se considera el directorio donde se esta actualmente, es decir, el directorio de trabajo actual, siendo lo mismo que indicarlo con un punto “.”.
* Es posible asignar mas de una ruta de búsqueda, por ejemplo: `find /etc /usr /var -group admin`.
* Algunas banderas que se pueden utilizar:
  * `-name`. Busca por el nombre de un archivo.
  * `-iname`. Igual que `name` pero sin distinguir entre mayúsculas y minúsculas.
  * `-user`. Busca por usuario propietario.
  * `-group`. Busca por grupo propietario.
  * `-type`. Busca por tipo de archivo. `f` corresponde a *Regular File*, es decir solo buscará archivos, mientras `d` corresponde a *Directorios*.
* Búsqueda a través del tiempo:
  * `-mmin`. Tiempo en minutos.
  * `-mtime`. Periodos de 24 horas.

---
## Herramientas para interactuar a través de HTTP <a name="HTTP"/> [↑](#inicio)
### Comandos HTTP
Se tienen básicamente dos herramientas para utilizar en servidores HTTP:

Comando | Descripción | Ejemplos
--------|-------------|---------
**`curl`** | Se utiliza para hacer *"pedidos crudos"*, es decir, se hace una petición a un servidor y se recibe la respuesta mostrandola en pantalla. | <ul><li>`curl https://vainilladev.com` Devuelve el código HTML de la página ingresada</li><li>`curl -v https://vainilladev.com \| more` No solo muestra el HTML que devuelve el servidor, si no toda   la comunicación vía HTTP. Se utiliza la herramienta `more` para mostrar el resultado en secciones. Presionando 'Enter' muestra el siguiente resultado línea por línea, presionando la barra espaciadora ('Space') muestra otra pantalla o sección</li><li>`curl -v https://vainilladev.com > /dev/null` Permite ver solamente los encabezados. Redirecciona la salida standar hacía un "archivo" de las consolas de Linux, llamado *"El agurero negro"*, que realmente no es un archivo, simplemente es para no llenar la pantalla con información que no se desea visualizar.</li></ul>
**`wget`** | Es un poco más compleja ya que permite realizar descargas. | `wget http://www.php.net/distributions/php-7.3.10.tar.bz2` Se hace la descarga del archivo binario seleccionado, para este caso la distribución *php-7.3.10.tar.bz2*


---
## Acceso seguro a otras computadoras <a name="ssh"/> [↑](#inicio)
Utilizar conexiones seguras evita que "agentes maliciosos (hackers)" puedan captar o interceptar la información compartida entre equipos.
La seguridad de estás conexiones se basa en la encripción, es decir, los comandos no viajan en texto plano, literalmente como fueron escritos, sino que pasan por un proceso de modificación, logrando así que la información, aún si es captada, sea muy difícil, prácticamente imposible, de ser interpretada por una persona.

### ssh
El comando utilizado para la conexión remota es **`ssh` Secure SHell** nombre de un protocolo y del programa que lo implementa, cuya principal función es el acceso remoto a un servidor por medio de un canal seguro en el que toda la información está cifrada.
Los parametros del comando son:
* El usuario y
* El host al que sea desea conectar

Ejemplo: `ssh leeway-prod` Conexión simplificada y configurada hacía el propio sitio.


---
## Servicio de mailing <a name="Mailing"/> [↑](#inicio)
### postfix
Es un servidor de correo, un programa informático para el enrutamiento y envío de correo electrónico, creado con la intención de que sea una alternativa más rápida, fácil de administrar y segura al ampliamente utilizado *Sendmail*. Se encarga de las tareas de comunicación con los servidores de destino.

Para instalar *postfix* se utiliza el comando `sudo DEBIAN_PRIORITY=low apt install postfix`.

Para la configuración se puede seguir la guía de [Configuración de un servicio de mailing](https://platzi.com/clases/1748-terminal-2019/24407-configuracion-de-un-servicio-de-mailing/)

### mail
Envía correos a través de la consola.

**NOTA:** Es necesario tener instalada la herramienta *mail* y hacer las configuraciones correspondientes. *mail* se puede obtener ejecutando el comando `sudo apt install mailutils`.

Por ejemplo: `echo "Probando" | mail -s "Probando correo enviado desde consola" mvaldes988@gmail.com`


---
## Variables de entorno <a name="Variables-entorno"/> [↑](#inicio)
Es una definición global a la que todos los procesos tienen acceso, especifícamente el Sistema Operarivo, aunque la Terminal también tiene acceso a esas variables para tomar más información sobre lo que esta pasando alrededor.

Es importante saber que todos los procesos tiene acceso a estas variables, tanto para leerlas como para modificarlas.

Con la siguiente serie de comandos se puede entender como funcionan las variables de entorno para la ejecución de los comandos sin necesidad de utilizar su ruta completa.
`whereis echo`, `echo "Hola"`, `/bin/echo "Hola"`

### Asignación
Se pueden hacer para:
* Un comando en particular o
* Para toda la sesión (Asignación Global).

#### Asignación Particular
Definen el valor de una variable solamente para el próximo comando que será ejecutado, no es muy usual, pero puede ser utilizado para sobreescribir variables ya definidas sin alterarlas para todos los comandos, sino solo para una ejecución en particular. Por ejemplo: `MI_VAR=/home php env.php` o `PORT=3001 node start.js`.

#### Asignación Global
Para la asignación en toda la sesión se utiliza el comando `export`, como: `export VAR_NAME=valor`, por ejemplo: `export MI_VAR=Miguel`, por lo tanto *MI_VAR* quedará con el valor *Miguel* para todos los comandos utilizados en la sesión.

### Variable PATH
En esta variable se almacenan todas las rutas en las que el interprete de comandos (la terminal), tiene que ir a buscar los archivos ejecutables.

Esta variable esta definida sin nosotros definirla explicitamente, esto es debido a archivos ocultos que el Sistema Operativo ejecuta cada vez que se inicia sesión. Por ejemplo: `vim /etc/environment`. Nos muestra la defición del PATH, que a su vez nos dice todo lo que esta definido.

`echo $PATH`. El modificador `$` es una forma de pedirle al interprete de comandos que expanda el contenido de esa variable (en este caso *PATH*). **NOTA: Esto sucede con todas las variables de entorno**

`ls /usr/bin -l | more`. Muestra la serie de comandos ubicados en la PATH (Ruta) /usr/bin.


---
## Automatización de tareas, ¿Cómo y para qué escribir scripts en Bash <a name="Automatización"/> [↑](#inicio)
El Shell (el Bash), es a su vez un interprete de comandos y un lenguaje de programación, permitiendo así hacer algunos scripts, uilizando la misma sintaxis de Bash, estos scripts lo que harán, es simplemente llamar a otros comandos, evaluar condiciones y ejecutar con base en eso, permitiendo así crear comandos propios.

**Creando un script:**
1. `vim platzi.sh `. Se crea el script desde la utilidad *vim*
2. *i*. Para ingresar en el modo inserción del *vim*
3. `#!/usr/bin/bash`. Se agrega el interprete. Después de esta línea los comandos insertados serán las acciones que se reproducirán al ejecutar el script 
4. `NEW_DIR=platzi`. Creación de una variable *"Nuevo directorio"*
5. `if [ ! -d "/root/$NEW_DIR" ]; then mkdir /roor/$NEW_DIR fi`. Se traduce a: *Si no existe el directorio '/root/platzi' entonces se creará*. `-d` Es un comando que permite determinar la existencia de un directorio. `!` al igual que en muchos lenguajes de programación, es utilizado para negar condiciones. `fi` indica el cierre del condicionante (o la instrucción) `if`.
5. `cp file1.txt /root/$NEW_DIR/` Se copia un archivo cualquiera al nuevo directorio.
6. `echo "`\``date`\``: Todo listo Jefe!"`. Solo se muestra la fecha seguida del mensaje *"Todo listo Jefe!"*.
7. Presionar *esc* y agregar *:x*. Para guardar los cambios y salir de *vim*.

**Ejecución del script:**
1. `./platzi.sh`. Permiso Denegado. Debido a que no se tiene permiso de ejecución.
2. `ls platzi.sh -l`. Revisamos los permisos.
3. `chmod u+x platzi.sh`. Otorgamos permisos de ejecución para el dueño del archivo.
4. `./platzi.sh`. Volvemos a ejecutar.

### Modificando la Configuración General del Sistema
*etc/environment*, es un archivo de configuración del sistema, cuando el sistema inicia se levanta este archivo y algunos otros que se van concatenando y van armando una nueva versión de la variable PATH. Si ejecutamos el comando `vim /etc/environment`, nos muestra la defición del PATH, que a su vez nos dice todo lo que esta definido (variables de entorno agregadas).

Si se corre el comando `vim .bashrc`, se podrá editar el archivo *.bashrc* que se ejecuta cada inicio de sesión en la terminal, entoces se podría hacer algo como:
1. *i*. Para entrar al modo de inserción del *vim*.
2. `export PATH=$PATH:/home/miguel/Miguel`.
3. Presionar *esc* y agregar *:x*. Para guardar los cambios y salir de *vim*.
4. Para visualizar los cambios y esto comience a tener sentido se debe correr el comando `source .bashrc`
5. `echo $PATH`. Para visualizar los cambios, y observar que se agregó *:/home/miguel/Miguel*

Como el archivo es el *.bashrc* esta nueva definición se ejecutará cada vez que se inicie una nueva sesión, de modo que así se ha **modificado la configuración general del sistema.**

### ¿Cómo y para qué dejar tareas programadas?
La creación de nuestros propios comandos es sumamente útil para evitar repetir constantemente lo mismo pero ciertamente la mayor utilidad es dejar ***tareas programadas***, de modo que se ejecuten mientras podemos hacer otra cosa.

#### Programación de tareas
Se cuenta con las siguientes dos utilidades:

##### `at`
Permite ejecutar comandos desde el entorno *at* después de un período indicado.

Ejemplo. `at now +2 minutes` después `echo "Hola Mundo!" > /home/miguel/hola.txt` presionar *ctrl + d* para terminar.

Esta seríe de comandos indica que se escribirá un *"Hola Mundo!"* en el archivo *hola.txt* (si no existe el archivo entonces se creará) después de dos minutos a partir de ahora. Al correr el comando entramos a algo así como un entorno de trabajo de *at* donde se agrega lo que se desea ejecutar; para indicar que ya terminamos lo hacemos presionando *ctrl + d*.

##### `cron`
No es necesario invocar el comando `at`, ya que se pueden dejar comandos programados para su ejecución de forma periódica. `cron` utiliza un archivo que es el *crontab*, la tabla de Cron Shops o de Tareas Programadas que tiene una sintaxis un poco particular, Ejemplo:
* `crontab -e`. Permite editar, es decir, ver las tareas que están programadas y crear nuevas tareas. Para crear los comandos se debe seguir una estructura definida en el mismo *crontab*:
  * El primer elemento (m) es el minuto en el que debe ejecutarse un comando, 
  * El segundo elemento (h) es la hora, 
  * El tecero (dom) es el día del mes, 
  * El cuarto (mon) es para indicar que mes se ejecutará
  * El quinto (dow) representa el día de la semana y
  * Por último se debe agregar el comando que se quiera ejecutar

Ejemplo:  `45 12 * * * echo "Hola" > hola2.txt`. Este ejemplo, lo que hará es escribir un *"Hola"* en el archivo *hola2.txt* a las 12:45 de todos los días de todos los meses.

El signo '*' representa que abarca todo lo referente a ese elemento, por ejemplo en el campo del día, inidicaría que se tomarán en cuenta todos los días.


**NOTA:** Si NO funcionó el `at` y responde con esto *“Can’t open /var/run/atd.pid to signal atd. No atd running?”* Es necesario activar los servicios atd y cron de la siguiente manera:

* Ejecutar `service --status-all` para ver si los servicios están activos 
* Para activar cada uno de los servicios, ejecutar: `sudo service atd start` y `sudo service cron start`

Finalmente repiter el primer comando donde deberá salir un signo '+' en esos servicios.
