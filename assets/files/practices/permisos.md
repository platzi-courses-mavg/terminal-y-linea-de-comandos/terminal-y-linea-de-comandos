# Práctica: Usuarios, Grupos y Permisos [⮌](../../../README.md)
* **¿Cuáles acciones y permisos puede tener un usuario sobre un archivo?** Lectura (r), Escritura (w), y Ejecución (x).
​
* **¿Todos los usuarios tienen los mismos permisos sobre todos los archivos? ¿Quiénes pueden o no trabajar con estos documentos? ¿Cómo se dividen los permisos de un archivo o carpeta?** Todos los archivos de Unix tienen tres **usuarios**: Un *dueño* asociado, creador del archivo, un *grupo* de usuarios que pueden accederlo y *Otros* usuarios a los que se les puede dar permisos distintos. Al correr el comando `ls -l`, se pueden visualizar los permisos de *Lectura (r)*, *Escritura (w)* y *Ejecución (x)* para cada uno de los usuarios (grupos de tres caracteres (rwx)) *dueño* (posición 1), *grupo* (posición 2) y *otros (o)* (posición 3) en la primer columna de la salida del comando; el primer carácter de esta columna indica el tipo de archivo, por ejemplo: *d* indica que el archivo es un directorio, *-* para archivos comunes y *l* para links (puntero a otro archivo). Cuando uno de los permisos (rwx) es sustituido por un *'-'* significa que no se puede utilizar ese permiso.
​
* **Al hablar de usuarios y permisos en el sistema operativo, ¿qué significa un 777? ¿Deberíamos preocuparnos?** Al ser utilizado con el comando `chmod`, indica que el *Dueño* del archivo, el *Grupo* y *Otros* tendrán los tres permisos, *Lectura (r)*, *Escritura (w)* y *Ejecución (x)* sobre el archivo especificado en el comando.
​
* **¿Que permisos debes tener para poder editar un archivo que creo otro usuario de tu sistema operativo?** De Lectura (r) y Escritura (w)
​
* **¿Con qué comando cambiamos los permisos que tienen los usuarios para trabajar con un archivo?** `chmod 'permiso' 'NombreDelArchivo`. Ejemplos: `chmod o-w file1.txt` Quita el permiso de *Escritura* a *Otros* para el archivo *file1.txt* o `chmod +x practice.txt` Agrega el permiso de *Ejecución* a cualquiera de los tres usuarios para el archivo *practice.txt*
​
* **No quieres que nadie pueda editar ni mucho menos ejecutar un script muy peligroso que estás creando. ¿Qué permisos debe tener este script para conseguir este resultado?** 600 para que solo el dueño (yo) pueda verlo y editarlo, pero NO ejecutarlo. **Comando:** `chmod 600 fileName`.
​
* **¿Quién es sudo? ¿Es peligroso?** Es una herramienta que permite ejecutar comandos con permisos "especiales", permisos de usuario *root*. El `sudo` debe anteponerse al comando que se desee ejecutar.