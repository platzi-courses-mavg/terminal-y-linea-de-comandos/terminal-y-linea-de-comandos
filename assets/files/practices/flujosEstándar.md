### Práctica: Comunicación entre Procesos [⮌](../../../README.md)
El desafío de esta clase consiste en crear un archivo con al menos 30 líneas de texto del lenguaje de programación que más te guste.

Luego de esto, crea dos nuevos archivos con el contenido de este archivo, uno con la primera mitad y el otro con la segunda mitad. Pero no puedes usar un editor de texto ni copiar el texto a mano, debes usar los comandos que estudiamos en clases anteriores para obtener el resultado que necesitas y enviarlo al archivo indicado.

Finalmente, usa el comando date para obtener la fecha y hora de hoy. Y añade ese resultado al final del primer archivo que creaste (el que debía tener al menos 30 líneas). No puedes copiar y pegar, debes hacerlo directamente desde la terminal y sin editores de texto. Pero ten mucho cuidado de no borrar el resto de contenido de tu archivo.

#### Solución:
1. Se creo el archivo para la práctica. `nano practice.txt`
2. Se agrego el siguiente contenido al archivo *practice.txt* desde nano 
```
Alonso
Berenice
Carlos
Denisse
Ernesto
Favio
Gerardo
Heraldo
Issac
Jorge
Karen
Luis
Miguel
Nicolas
Oscar
Patricio
Quetzalcoatl
```
3. Se guardo el contenido y se cerro nano con los siguientes comandos: `^O` y `^X`
4. Se creo el primer archivo a partir del archivo *practice.txt*. `head -n 8 practice.txt > file1.txt`
4. Se creo el segundo archivo a partir del archivo *practice.txt*. `tail -n 9 practice.txt > file2.txt`
5. Se agrego la fecha y hora al archivo *practice.txt*. `date >> practice.txt`
6. Para observar los archivos y revisar los resultados se corrieron los siguientes comandos: `cat practice.txt`, `cat file1.txt` y `cat file2.txt`